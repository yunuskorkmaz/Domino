﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Domino.Annotations;

namespace Domino
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private int nrows = 4, ncolumns = 4;

        public ObservableCollection<Rectangle> Cells { get; set; }

        public int NRows
        {
            get
            {
                return nrows;
            }
            set
            {
                nrows = value;
               
                OnPropertyChanged(nameof(NRows));
            }
        }
        public int NColumns
        {
            get
            {
                return ncolumns;
            }
            set
            {
                ncolumns = value;
                
                OnPropertyChanged(nameof(NColumns));
            }
        }

        

        private async void CreatePanel()
        {
           
            if (Cells == null)
            {
                Cells = new ObservableCollection<Rectangle>();
            }
            else
            {
                Cells.Clear(); 
            }
            int ncells = NRows * NColumns;
            for (int i = 0; i < ncells; i++)
            {
                var a = new Rectangle();
                a.Width = 10;
                a.Height = 40;
             
                a.Fill = new SolidColorBrush(Colors.Red);
                a.Margin = new Thickness(5);
                Cells.Add( a);
            }


            for (var row = 0; row < NRows; row++)
            {
                if (row % 2 == 0)
                {
                    var rowstart = row * NColumns;

                    for (var i = rowstart; i < rowstart + NColumns; i++)
                    {
                        await animation(i);
                    }
                }
                else
                {
                    var rowstart = row * NColumns;
                    for (var i = rowstart +NColumns -1; i >= rowstart ; i--)
                    {
                        await animation(i,-80);
                    }
                }

                
            }

        }

        public MainWindow()
        {
            InitializeComponent();
           
        }

        public async Task animation(int i,int dec = 88)
        {
           
            var bir = Cells[i] as Rectangle;
            DoubleAnimation da = new DoubleAnimation();
            da.From = 0;
            da.To = dec;
            da.Duration = new Duration(TimeSpan.FromMilliseconds(500));

            RotateTransform rt = new RotateTransform()
            {
                CenterY = bir.RadiusY + bir.Height
            };
            
            bir.RenderTransform = rt;
            rt.BeginAnimation(RotateTransform.AngleProperty, da);
            await Task.Delay(300);
        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DataContext = this;
            CreatePanel();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                CreatePanel();
            }
        }
    }
}
